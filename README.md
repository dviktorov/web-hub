# WEB HUB #

Web Hub is a Java web application which is intended to simplify the creation
of various REST APIs as well as to provide essential functionality needed at 
the web application, especially networking.

### More about Web Hub ###

The project is based on Gradle which includes Groovy scripts to assemble
the web application and run it with Jetty 9.

The application deploys Spring Framework 4.x on start and uses its MVC module
for the main logic. Any other Spring modules or dependencies can be added to 
build.gradle whenever needed.

### How to run ###

1. You need **git** and **gradle** installed at your system and available in the PATH variable.
1. Clone the project with: **git clone https://bitbucket.org/dviktorov/web-hub.git**
2. Run this command from the cloned folder: **gradle runJettyPlug**. You may also use
the **run.sh** script included in the folder. This one will also open a remote debugger.
3. To execute tests run the following command from the cloned folder: **gradle test**

### License ###

This program follows the Apache License version 2.0 (http://www.apache.org/licenses/)

### Who do I talk to? ###

* Dmitry Viktorov - Please report any issues you may find or ideas for improvements


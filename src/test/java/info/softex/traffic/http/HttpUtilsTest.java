/*
 * Web Hub is an open web application with various embedded basic functionality.
 *
 * 2015  Dmitry Viktorov <dmitry.viktorov@softex.info> <http://www.softex.info>
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.softex.traffic.http;

import com.google.common.collect.Lists;
import com.google.common.net.HttpHeaders;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Tests for <code>HttpUtils</code>.
 *
 * @since     version 1.0,    03/28/2015
 *
 * @modified  version 1.1,    10/02/2015
 *
 * @author  Dmitry Viktorov
 *
 */
public class HttpUtilsTest {

    protected final static String TEST_UA = "Test User Agent";
    protected final static String TEST_REFERER = "Test Referer";
    protected final static String TEST_PRAGMA_1 = "Test Pragma 1";
    protected final static String TEST_PRAGMA_2  = "Test Pragma 2";
    protected final static String HEADER_PRAGMA_RANDOM_CASE = "PragMa";

    protected MockHttpServletRequest testRequest;

    protected Map<String, Object> testHeaders;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void doBefore() {
        testRequest = new MockHttpServletRequest();
        testRequest.addHeader(HttpHeaders.USER_AGENT, TEST_UA);
        testRequest.addHeader(HttpHeaders.PRAGMA, TEST_PRAGMA_1);
        testRequest.addHeader(HttpHeaders.PRAGMA, TEST_PRAGMA_2);

        testHeaders = new LinkedHashMap<String, Object>(HttpUtils.getHttpHeaders(testRequest));
        testHeaders.put(HttpHeaders.REFERER, TEST_REFERER);

        Assert.assertEquals(3, testHeaders.size());
    }

    @Test
    public void testFirstHttpHeader() {
        Assert.assertEquals(TEST_PRAGMA_1,  HttpUtils.getFirstHeaderIgnoreCase(testHeaders, HEADER_PRAGMA_RANDOM_CASE));
        Assert.assertEquals(TEST_REFERER,  HttpUtils.getFirstHeaderIgnoreCase(testHeaders, HttpHeaders.REFERER));
    }

    @Test
    public void testRemoveHttpHeaders() {
        int initHeadersSize = testHeaders.size();
        HttpUtils.removeHeaderIgnoreCase(testHeaders, HEADER_PRAGMA_RANDOM_CASE);

        Assert.assertEquals(initHeadersSize - 1, testHeaders.size());
        Assert.assertNull(testHeaders.get(HEADER_PRAGMA_RANDOM_CASE));
        Assert.assertNull(testHeaders.get(HttpHeaders.PRAGMA));
    }

    @Test
    public void testRequestHttpHeadersNull() {
        thrown.expect(NullPointerException.class);
        HttpUtils.getHttpHeaders(null);
    }

    @Test
    public void testRequestHttpHeadersEmpty() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        Map<String, List<String>> headers = HttpUtils.getHttpHeaders(request);
        Assert.assertNotNull(headers);
        Assert.assertTrue(headers.isEmpty());
    }

    @Test
    public void testRequestHttpHeadersNonEmpty() {
        Assert.assertEquals(TEST_UA, HttpUtils.getFirstHeaderIgnoreCase(testHeaders, HttpHeaders.USER_AGENT));
        Assert.assertEquals(TEST_PRAGMA_1, HttpUtils.getFirstHeaderIgnoreCase(testHeaders, HttpHeaders.PRAGMA));
        Assert.assertEquals(TEST_PRAGMA_2, ((List) testHeaders.get(HttpHeaders.PRAGMA)).get(1));
    }

    @Test
    public void testResponseHttpHeadersNull() {
        thrown.expect(NullPointerException.class);
        MockHttpServletResponse response = new MockHttpServletResponse();
        HttpUtils.setHttpHeaders(response, null);
    }

    @Test
    public void testResponseHttpHeaders() {
        MockHttpServletResponse response = new MockHttpServletResponse();
        HttpUtils.setHttpHeaders(response, testHeaders);

        Assert.assertEquals(TEST_UA, response.getHeader(HttpHeaders.USER_AGENT));
        Assert.assertEquals(TEST_PRAGMA_1, response.getHeader(HttpHeaders.PRAGMA));
        Assert.assertEquals(Lists.newArrayList(TEST_PRAGMA_1, TEST_PRAGMA_2), response.getHeaders(HttpHeaders.PRAGMA));
    }

}


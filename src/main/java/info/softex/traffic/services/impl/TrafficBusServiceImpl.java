/*
 * Web Hub is an open web application with various embedded basic functionality.
 *
 * 2015  Dmitry Viktorov <dmitry.viktorov@softex.info> <http://www.softex.info>
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.softex.traffic.services.impl;

import info.softex.traffic.http.HttpDataEntity;
import info.softex.traffic.services.TrafficBusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Service for proxying and traffic multiplication.
 *
 * @since   version 1.0,    03/21/2015
 *
 * @author  Dmitry Viktorov
 *
 */
@Service
public class TrafficBusServiceImpl implements TrafficBusService {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    protected final ExecutorService executor = Executors.newFixedThreadPool(512);
    
    @Autowired
    private RestTemplate rest;

    @Override
    public void pushAsyncRequest(HttpDataEntity inHttpEntity) {
        Callable<byte[]> task = new RequestCallable(inHttpEntity);
        executor.submit(task);
    }

    @Override
    public HttpDataEntity pushSyncRequest(HttpDataEntity inHttpEntity) {
        // Populate spring headers
        HttpHeaders springHeaders = new HttpHeaders();
        for (Map.Entry<String, List<String>> entry : inHttpEntity.getHeaders().entrySet()) {
            //log.debug("Header -> Key: {}, Value: {}", entry.getKey(), entry.getValue());
            springHeaders.put(entry.getKey(), entry.getValue());
        }

        // Make a request and get body
        HttpEntity<String> entity = new HttpEntity<>(springHeaders);
        ResponseEntity<byte[]> response = rest.exchange(inHttpEntity.getPath(), HttpMethod.GET, entity, byte[].class);

        HttpDataEntity httpData = HttpDataEntity.create().
            path(inHttpEntity.getPath()).
            headers(response.getHeaders()).
            body(response.getBody());

        return httpData;
    }

    private class RequestCallable implements Callable<byte[]> {

        private final HttpDataEntity httpEntity;

        public RequestCallable(HttpDataEntity inHttpEntity) {
            this.httpEntity = inHttpEntity;
        }

        @Override
        public byte[] call() throws Exception {
            log.debug("Pushing async request to: {}", httpEntity.getPath());
            HttpDataEntity httpData = pushSyncRequest(httpEntity);
            if (httpData.getBody() != null) {
                log.debug("Response for async request: {}", new String(httpData.getBody()));
            }
            return httpData.getBody();
        }

    }

}

/*
 * Web Hub is an open web application with various embedded basic functionality.
 *
 * 2015  Dmitry Viktorov <dmitry.viktorov@softex.info> <http://www.softex.info>
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.softex.traffic.controllers;

import com.google.common.net.HttpHeaders;
import info.softex.traffic.http.HttpDataEntity;
import info.softex.traffic.http.HttpUtils;
import info.softex.traffic.services.TrafficBusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Controller for traffic multiplication and proxying.
 *
 * @since   version 1.0,    03/21/2015
 *
 * @author  Dmitry Viktorov
 *
 */
@Controller
@RequestMapping(value="/")
public class TrafficBusController {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Value("${traffic.bus.hosts.1}")
    private String primHost;

    @Value("${traffic.bus.hosts.2}")
    private String secHost;

    @Autowired
    private TrafficBusService bus;

    @Autowired
    private RestTemplate rest;

    @RequestMapping(value="/**", method = RequestMethod.GET)
    @ResponseBody
    public void proxy(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String path = request.getPathInfo();

        log.debug("Requested path: {}", path);

        String primFullPath = primHost + path;
        String secFullPath = secHost + path;

        Map<String, List<String>> headers = HttpUtils.getHttpHeaders(request);

        // Remove host header
        headers.remove(HttpHeaders.HOST);

        bus.pushAsyncRequest(HttpDataEntity.create().path(secFullPath).headers(headers));

        HttpDataEntity outHttpData = bus.pushSyncRequest(HttpDataEntity.create().path(primFullPath).headers(headers));

        HttpUtils.setHttpHeaders(response, outHttpData.getHeaders());

        try (ServletOutputStream os = response.getOutputStream()) {
            byte[] body = outHttpData.getBody();
            if (body != null) {
                os.write(body);
                os.flush();
            }
        } catch (IOException e) {
            log.error("Failed to write response to output", e);
            throw e;
        }

    }

}

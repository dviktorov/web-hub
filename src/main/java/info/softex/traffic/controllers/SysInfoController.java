/*
 * Web Hub is an open web application with various embedded basic functionality.
 *
 * 2015  Dmitry Viktorov <dmitry.viktorov@softex.info> <http://www.softex.info>
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.softex.traffic.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @since   version 1.0,    03/21/2015
 *
 * @author  Dmitry Viktorov
 *
 */
@Controller
@RequestMapping(value="/sysinfo")
public class SysInfoController {

    @RequestMapping(value="/health", method = RequestMethod.GET)
    @ResponseBody
    public String getHealth() {
        return "SYSTEM IS UP";
    }

    @RequestMapping(value="/container", method = RequestMethod.GET)
    @ResponseBody
    public String getServletVersion() {
        String servVersion = getClass().getPackage().getImplementationVersion();
        return "Version: " + servVersion;
    }

}

package info.softex.traffic.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 *
 * @since   version 1.0,    03/21/2015
 *
 * @author  Dmitry Viktorov
 *
 */
@Configuration
public class MVCConfig extends WebMvcConfigurationSupport {

//    @Override
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//        converters.add(converter());
//    }

//    @Bean
//    MappingJackson2HttpMessageConverter converter() {
//        // [...]
//    }

}
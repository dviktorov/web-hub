/*
 * Web Hub is an open web application with various embedded basic functionality.
 *
 * 2015  Dmitry Viktorov <dmitry.viktorov@softex.info> <http://www.softex.info>
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.softex.traffic.http;

import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * Convenience utilities for HTTP.
 *
 * @since     version 1.0,    03/28/2015
 *
 * @modified  version 1.1,    10/02/2015
 *
 * @author  Dmitry Viktorov
 *
 */
public class HttpUtils {

    /**
     * Extracts the HTTP headers into a map.
     *
     * @param inRequest
     * @return
     */
    public static Map<String, List<String>> getHttpHeaders(HttpServletRequest inRequest) {

        Preconditions.checkNotNull(inRequest, "Request can't be null");

        Map<String, List<String>> result = new LinkedHashMap<>();

        Enumeration<String> headers = inRequest.getHeaderNames();
        if (headers != null) {
            while (headers.hasMoreElements()) {
                String key = headers.nextElement();
                List<String> valuesList = new LinkedList<>();

                // Transfer values to the list
                Enumeration<String> values = inRequest.getHeaders(key);
                if (values != null) {
                    while (values.hasMoreElements()) {
                        String curValue = values.nextElement();
                        if (curValue != null) {
                            valuesList.add(curValue);
                        }
                    }
                }

                result.put(key, valuesList);
            }
        }

        return result;
    }

    /**
     * Finds the header ignoring its case and returns the first one if it's a list of headers.
     *
     * @param headers
     * @param headerKey
     */
    public static String getFirstHeaderIgnoreCase(Map<String, ?> headers, String headerKey) {
        if (headers != null && headerKey != null) {
            Set<String> headerKeys = new LinkedHashSet<>(headers.keySet());
            for (String curKey : headerKeys) {
                if (headerKey.equalsIgnoreCase(curKey)) {
                    Object curValue = headers.get(curKey);
                    if (curValue instanceof List) {
                        List curList = (List)curValue;
                        if (curList.size() > 0 && curList.get(0) != null) {
                            return curList.get(0).toString();
                        }
                    } else if (curValue != null) {
                        return curValue.toString();
                    }
                }
            }
        }
        return null;
    }

    /**
     * Removes the header ignoring its case.
     *
     * @param headers
     * @param headerKey
     */
    public static void removeHeaderIgnoreCase(Map<String, ?> headers, String headerKey) {
        if (headers != null && headerKey != null) {
            Set<String> headerKeys = new LinkedHashSet<>(headers.keySet());
            for (String curKey : headerKeys) {
                if (headerKey.equalsIgnoreCase(curKey)) {
                    headers.remove(curKey);
                }
            }
        }
    }

    /**
     * Sets HTTP headers to the response from the provided map.
     *
     * @param inResponse
     * @param inHeaders
     * @return
     */
    public static void setHttpHeaders(HttpServletResponse inResponse, Map<String, ?> inHeaders) {

        Preconditions.checkNotNull(inResponse, "Response can't be null");
        Preconditions.checkNotNull(inHeaders, "Headers can't be null");

        for (Map.Entry<String, ?> entry : inHeaders.entrySet()) {
            Object curHeaderValue = entry.getValue();
            if (curHeaderValue instanceof List) {
                List curHeaderList = (List) curHeaderValue;
                for (Object curHeader : curHeaderList) {
                    curHeader = (curHeader != null) ? curHeader : "";
                    inResponse.addHeader(entry.getKey(), curHeader.toString());
                }
            } else {
                curHeaderValue = (curHeaderValue != null) ? curHeaderValue : "";
                inResponse.addHeader(entry.getKey(), curHeaderValue.toString());
            }
        }

    }

}
/*
 * Web Hub is an open web application with various embedded basic functionality.
 *
 * 2015  Dmitry Viktorov <dmitry.viktorov@softex.info> <http://www.softex.info>
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.softex.traffic.http;

import java.util.List;
import java.util.Map;

/**
 * DTO that can be used for HTTP requests or responses.
 *
 * @since   version 1.0,    03/28/2015
 *
 * @author  Dmitry Viktorov
 *
 */
public class HttpDataEntity {

    private String path = null;

    private Map<String, List<String>> headers;

    private byte[] body = null;

    public String getPath() {
        return path;
    }

    public byte[] getBody() {
        return body;
    }

    public Map<String, List<String>> getHeaders() {
        return headers;
    }

    public static HttpDataEntity create() {
        return new HttpDataEntity();
    }

    public HttpDataEntity path(String inPath) {
        this.path = inPath;
        return this;
    }

    public HttpDataEntity headers(Map<String, List<String>> inHeaders) {
        this.headers = inHeaders;
        return this;
    }

    public HttpDataEntity body(byte[] inBody) {
        this.body = inBody;
        return this;
    }

}

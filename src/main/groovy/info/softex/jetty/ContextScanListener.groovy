/*
 * Web Hub is an open web application with various embedded basic functionality.
 *
 * 2015  Dmitry Viktorov <dmitry.viktorov@softex.info> <http://www.softex.info>
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.softex.jetty

import org.eclipse.jetty.util.Scanner
import org.eclipse.jetty.webapp.WebAppContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: Dmitry Viktorov
 * Date: 4/23/14
 * Time: 5:22 PM
 */
class ContextScanListener implements Scanner.BulkListener {

    final Logger log = LoggerFactory.getLogger(getClass());

    final WebAppContext wac;

    public ContextScanListener(WebAppContext inWac) {
        this.wac = inWac;
    }

    @Override
    void filesChanged(List<String> filenames) throws Exception {

        log.info("****** Restart application by listener...");
        log.info("Changed files: {}", filenames);
        wac.stop();
        wac.start();
        Thread.sleep(1000); // Sleep for stability of deploy

//        // Part redeplpoy
//        if (filenames.size() > 1) {
//
//            log.info("******Restart application by listener...");
//            wac.stop();
//            createClassDir(classDirListener, webappDirListener);
//            wac.start();
//            Thread.sleep(1000) ; //sleep for stability of deploy
//            deleteClassDir(webappDirListener);
//
//        } else if (filenames.size() == 1) {
//
//            String fileChange = filenames.get(0);
//            if (fileChange.endsWith(".war")) {
//                log.info("******Restart dependent application by listener...");
//                WebAppContext ctxToRestart = JettyMultiRun.getContextApp(fileChange, webDeps);
//                ctxToRestart.stop();
//                ctxToRestart.start();
//            } else {
//                log.warn("******Restart application by listener (filenames.size==1)??...");
////                        webapp.stop();
////                        createClassDir(classDirListener, webappDirListener);
////                        webapp.start();
////                        deleteClassDir(webappDirListener) ;
//            }
//        }
    }
}

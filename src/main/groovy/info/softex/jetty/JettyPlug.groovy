/*
 * Web Hub is an open web application with various embedded basic functionality.
 *
 * 2015  Dmitry Viktorov <dmitry.viktorov@softex.info> <http://www.softex.info>
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package info.softex.jetty

import org.eclipse.jetty.server.Server
import org.eclipse.jetty.util.Scanner
import org.eclipse.jetty.webapp.WebAppContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Created with IntelliJ IDEA.
 * User: Dmitry Viktorov
 * Date: 4/23/14
 * Time: 5:10 PM
 */
class JettyPlug {

    static final Logger log = LoggerFactory.getLogger(JettyPlug.class);

    public static void main(String[] args) {

        int serverPort = getSystemPropertyInt("web.server.port", 8080);
        log.info("Server Port: {}", serverPort);

        String webappDir = getSystemProperty("web.app.directory", null);
        if (webappDir == null) {
            log.error("Webapp directory must be submitted to run Jetty");
            return;
        }
        String webDescriptor = webappDir + "/WEB-INF/web.xml";
        log.info("Webapp Dir: {}", webappDir);

        String contextPath = getSystemProperty("web.app.context.path", "/");
        log.info("Webapp Context Path: {}", contextPath);

        String extraClassDir = getSystemProperty("web.app.class.dir", null);
        log.info("Webapp Class Dir: {}", extraClassDir);

        int scanInterval = getSystemPropertyInt("web.app.scan.interval", 0);
        log.info("Webapp Scan Interval: {}", scanInterval);

        Server server = new Server(serverPort);
        WebAppContext wac = new WebAppContext();
        wac.setResourceBase(webappDir);
        wac.setDescriptor(webDescriptor);
        wac.setContextPath(contextPath);
        wac.setParentLoaderPriority(false);
        if (extraClassDir != null) {
            wac.setExtraClasspath(extraClassDir);
        }
        //wac.setExtraClasspath(classPathApp+","+resourceDir);

        server.setHandler(wac);

        // LISTENER -------------------------------------------------------------
        Scanner scan = new Scanner();
        scan.setScanInterval(scanInterval);
        scan.setReportExistingFilesOnStartup(false);
        scan.setRecursive(true);
        List<File> listenFile = new LinkedList<>();
        listenFile.add(new File(extraClassDir));
//        if (containsElements) {
//            deployDeps.each { nameWar -> listenFile.add(new File(nameWar)) }
//        }
        scan.setScanDirs(listenFile);

        scan.addListener(new ContextScanListener(wac));
        scan.start();

        try {

            server.start();

            log.info("Jetty server started");

            server.join();

        } catch (Exception e) {
            log.error("Error", e);
        }
    }

    private static String getSystemProperty(String key, String defValue) {
        return System.getProperty(key, defValue);
    }

    private static int getSystemPropertyInt(String key, int defValue) {
        int resultInt = defValue;
        String result = System.getProperty(key);
        if (result != null) {
            resultInt = result.toInteger();
        }
        return resultInt;
    }

}
